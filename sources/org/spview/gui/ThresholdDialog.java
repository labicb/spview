package org.spview.gui;

import javax.swing.*;
import java.util.regex.Pattern;

public class ThresholdDialog {
    private static final Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    public double getThreshold() {
        String result = (String) JOptionPane.showInputDialog(
                null,
                "Select the prediction threshold you want to apply.\nNegative value for no threshold.",
                "Prediction threshold",
                JOptionPane.PLAIN_MESSAGE,
                null,
                null,
                PanAff.getThreshold()
        );
        if (isNumeric(result))
            return Double.parseDouble(result);
        else {
            return -Double.MAX_VALUE;
        }
    }

    private static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }
}