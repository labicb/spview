package org.spview.gui;

import org.spview.filehandler.OpusFile;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class OpusDialog extends JFrame implements ActionListener {
    Object selected;

    public OpusDialog(String[] string, String initialSelectionValue) {
        JPanel panel = new JPanel();

        JButton button = new JButton("Information");
        button.addActionListener(this);
        setLayout(new BorderLayout());
        panel.add(button);

        selected = JOptionPane.showInputDialog(
                null, panel,
                "OPUS File selector",
                JOptionPane.PLAIN_MESSAGE,
                null,
                string,
                initialSelectionValue
        );
    }
    public Object getSelected() {
        return selected;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String Title = "<center><h1>File Information</h1></center>";
        String InsParam = OpusFile.InstrumentParameters();
        String AcqParam = OpusFile.AcquisitionParameters();
        String SampleParam = OpusFile.SampleOriginParameters();

        JEditorPane editorPane = new JEditorPane(
                "text/html",
                Title +
                        InsParam +
                        AcqParam +
                        SampleParam
        );
        editorPane.setEditable(false);
        editorPane.setCaretPosition(0);

        // Put the editor pane in a scroll pane.
        JScrollPane scrollPane = new JScrollPane(editorPane);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setPreferredSize(new Dimension(800, 600));
        JOptionPane.showMessageDialog(this, scrollPane, "Information", JOptionPane.PLAIN_MESSAGE, null);
    }
}