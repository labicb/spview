package org.spview.point;
/**
 * This class allows sorting experiment data following assignment order.
 */

public class ExpJsynPoint implements Comparable<Object> {

    private String faso;                                             // frequency mark
    private String saso;                                             // intensity mark
    private String jsyn;                                             // assignment
    private int    ipred;                                            // index of related prediction line
    private String comm;                                             // comment

    /**
     * Construct a new ExpJsynPoint.
     *
     * @param cfaso   frequency mark
     * @param csaso   intensity mark
     * @param cjsyn   assignment
     * @param cipred  index of related prediction line
     * @param ccomm   comment
     */
    public ExpJsynPoint(String cfaso, String csaso, String cjsyn, int cipred, String ccomm) {

        faso  =  cfaso;                                              // frequency mark
        saso  =  csaso;                                              // intensity mark
        jsyn  =  cjsyn;                                              // assignment
        ipred = cipred;                                              // index of related prediction line
        comm  = ccomm.trim();                                        // comment (without extra spaces)
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get frequency mark.
     */
    public String getFaso() {

        return faso;
    }

    /**
     * Set frequency mark.
     */
    public void setFaso(String cfaso) {

        faso =  cfaso;
    }

    /**
     * Get intensity mark.
     */
    public String getSaso() {

        return saso;
    }

    /**
     * Set intensity mark.
     */
    public void setSaso(String csaso) {

        saso =  csaso;
    }

    /**
     * Get related assignment.
     */
    public String getJsyn() {

        return jsyn;
    }
    /**
     * Set related assignment.
     */
    public void setJsyn(String cjsyn) {

        jsyn =  cjsyn;
    }

    /**
     * Get index of related prediction line.
     */
    public int getIpred() {

        return ipred;
    }

    /**
     * Set index of related prediction line.
     */
    public void setIpred(int cipred) {

        ipred =  cipred;
    }

    /**
     * Get comment.
     */
    public String getComm() {

        return comm;
    }

    /**
     * Set comment.
     */
    public void setComm(String cstr) {

        comm = cstr.trim();                                          // without extra spaces
    }

    /**
     * Compare assignment strings.
     *
     * @param cejpt the ExpJsynPoint to compare to
     */
    public int compareTo(Object cejpt) {

        if( ! (cejpt instanceof ExpJsynPoint) ) {                    // not the right object
            throw new ClassCastException();
        }
        int delta = ((ExpJsynPoint)cejpt).getJsyn().compareTo(jsyn);  // compare assignment strings
        if     ( delta < 0 ) {
            return  1;
        }
        else if( delta > 0 ) {
            return -1;
        }
        return 0;                                                    // equal
    }

}
