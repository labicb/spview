package org.spview.filehandler;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.swing.JOptionPane;

public class FortranFormat {

	/**
	 * Format a frequency. <br>
	 * F12.6 FORTRAN FORMAT <br>
	 * WARNING : only positive values
	 */
	public static String formFreq(double cx) {
		StringBuilder sb;

		NumberFormat nff = NumberFormat.getNumberInstance(Locale.US); // ask a plain format
		DecimalFormat dff = (DecimalFormat) nff; // reduce to decimal format
		
		dff.applyPattern("00000.000000"); // define pattern
		
		sb = new StringBuilder(dff.format(cx)); // format
		for (int i = 0; i < sb.length(); i++) {
			if (sb.charAt(i) == '0') {
				sb.setCharAt(i, ' '); // replace leading 0s with spaces
			} else {
				// end of leading 0s
				break;
			}
		}
		return sb.toString();
	}
	
	public static String formResidus(double cx) {
		StringBuilder sb;

		NumberFormat nff = NumberFormat.getNumberInstance(Locale.US); // ask a plain format
		DecimalFormat dff = (DecimalFormat) nff; // reduce to decimal format
		
		dff.applyPattern(".0000"); // define pattern
		
		sb = new StringBuilder(dff.format(cx)); // format
		if (sb.charAt(0) != '-') { // if not beginning with a -
			sb.insert(0, ' '); // insert a space
		}
		for (int i = 0; i < sb.length(); i++) {
			if (sb.charAt(i) == '0') {
				sb.setCharAt(i, ' '); // replace leading 0s with spaces
			} else {
				// end of leading 0s
				break;
			}
		}
		return sb.toString();
	}
	
	/**
	 * Format an intensity. <br>
	 * E11.4 FORTRAN FORMAT
	 */
	public static String formInt(double cy) {
		StringBuilder sb;
		int ie;
		
		NumberFormat nfi = NumberFormat.getNumberInstance(Locale.US); // ask a plain format
		DecimalFormat dfi = (DecimalFormat) nfi; // reduce to decimal format
		
		dfi.applyPattern(".0000E00"); // define pattern
		
		sb = new StringBuilder(dfi.format(cy)); // format
		if (sb.charAt(0) != '-') { // if not beginning with a -
			sb.insert(0, ' '); // insert a space
		}
		ie = sb.indexOf(".");
		if (ie >= 0) {
			sb.insert(ie, '0'); // insert a 0 before the .
		}
		ie = sb.indexOf("E");
		ie++;
		if (sb.charAt(ie) != '-') { // if positive exponent
			sb.insert(ie, '+'); // insert a +
		}
		return sb.toString();
	}

	// return an blank string
	public static String bStr(int nbc) {
		return " ".repeat(Math.max(0, nbc));
	}

	public static void printAssignmentFormattedOutput(ArrayList<Point2D> peakPositions, Point2D uncert, File file) {
		String lnsep = System.getProperty("line.separator");

		PrintWriter out1 = null;
		try {
			out1 = new PrintWriter(file);
			for (int i = 0; i < peakPositions.size(); i++) {
				double x = peakPositions.get(i).getX();
				double y = peakPositions.get(i).getY();

				// NUO (5 char, RIGHT)
				String cStrNUO = "     " + ("" + (i + 1)).trim();
				cStrNUO = cStrNUO.substring(cStrNUO.length() - 5);
				String cStr = (" " + // ISP
						cStrNUO + // NUO line number
						FortranFormat.formFreq(x) + // FOBS frequency
						"  " + FortranFormat.formInt(y) + // SOBS intensity
						"  " + String.format("%9.6f %4.1f", uncert.getX(), uncert.getY()));// SDFOBS,SDSOBS frequency
																							// and intensity
				// standard deviations string

				int l;
				for (l = cStr.length(); l > 0; l--) {
					if (cStr.charAt(l - 1) != ' ') {
						break;
					}
				}
				out1.println(cStr.substring(0, l)); // without extra spaces
			}
		} catch (IOException ioe) { // IO error
			JOptionPane.showMessageDialog(null, "IO error while writing file" + lnsep + ioe);
		} finally {
			// close the file
			if (out1 != null) {
				out1.close();
				if (out1.checkError()) {
					JOptionPane.showMessageDialog(null, "PrintWriter error while creating exp file" + lnsep);
				}
			}
		}
	}
}
